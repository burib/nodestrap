// DEPENDENCIES: built in nodeJS dependencies
var path = require('path');
var sys = require('sys');
var http = require('http');
var https = require('https');
// DEPENDENCIES: express and middlewares
var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var morgan = require('morgan');
var compression = require('compression');
var app = express();
// DEPENDENCIES: template engine
var swig = require('swig');
var env = process.env.NODE_ENV || 'development';

if ('development' == env) {
  // custom middleware to always get the latest static resources if environment is development.
  app.use(function (req, res, next) {
    res.header("Cache-Control", "no-cache, no-store, must-revalidate");
    res.header("Pragma", "no-cache");
    res.header("Expires", 0);
    res.header("X-Powered-By", "NodeStrap");
    next();
  });
}

// CONFIGURE: templating
app.engine('html', swig.renderFile); // assign the swig engine to .html files
app.set('view engine', 'html'); // set .html as the default extension
app.set('views', path.join(__dirname + '/views'));
app.set('port', process.env.PORT || 80);
app.set('view cache', false);
swig.setDefaults({
  loader: swig.loaders.fs(path.join(__dirname + '/views/layouts/')), // Tell swig where to look for templates when one extends another.
  allowErrors: true, // allows errors to be thrown and caught by express instead of suppressed by Swig,
  cache: false,
  locals: {} // pass local object. will be available in template
});
// CONFIGURE: logging
app.use(morgan());
morgan('dev');

app.get('/', function (req, res) {
  res.render("home");
});
//gzip compression for some reason is not really working :( will have to fix this.
//app.use('/', compression());

app.use(cookieParser());
app.use(bodyParser()); // get the data from a POST
app.use(methodOverride()); // simulate DELETE and PUT

// CONFIGURE: static resource usage
app.use(express.static(path.join(__dirname + '/public')));
// Handle 404
app.use(function (req, res) {
  res.status(404);
  res.render("error", {status: 404, errorMessage: 'Not found'});
});
// Handle 500
app.use(function (error, req, res, next) {
  res.status(500);
  res.render("error", {status: 500, errorMessage: error});
});

app.listen(app.get('port'));
sys.log("Express server listening on port " + app.get('port'));
sys.log("Node version: " + process.versions.node);
sys.log("Environment: " + app.get('env'));